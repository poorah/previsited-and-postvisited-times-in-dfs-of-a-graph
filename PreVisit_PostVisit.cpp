#include <bits/stdc++.h>

using namespace std ;

class Graph {
    public:
        // properties :
        map<int, bool> visited;
        map<int, list<int>> adj;
        //Nahveye zakhire kardane pre and post
        map<int, int> preNum;
        map<int, int> postNum;
        int clock = 1;
        //end
        //methods :
        void addEdge(int v, int w);
        void Explore(int v);
        void preVisit(int v);
        void postVisit(int v);
        void showPrePost();
        void DFS();
};

void Graph::addEdge(int v, int w) {
    //TODO
    adj[v].push_back(w);
    adj[w].push_back(v);
}

void Graph::Explore(int v) {
    //TODO
    visited[v] = true ;
    //jaygah mohem!
    preVisit(v);
    //end
    cout << v << " -> " ;
    list<int>::iterator i;

    for(i = adj[v].begin(); i != adj[v].end(); i++) {
        if(!(visited[*i])) {
            Explore(*i) ;
        }
    }
    //jaygah mohem
    postVisit(v);
    //end
}

void Graph::preVisit(int v) {
    preNum[v] = clock;
    clock++;
}

void Graph::postVisit(int v) {
    postNum[v] = clock;
    clock++;
}

void Graph::DFS() {
    map<int, list<int>>::iterator i;
    
    for(i = adj.begin(); i != adj.end(); i++) {
        if(!(visited[i->first])) {
            Explore(i->first) ;
            cout << "end" << endl ;
        }
    }
}
//nahve mohem
void Graph::showPrePost() {
    
    map<int, int>::iterator i;
    
    for(i = preNum.begin(); i != preNum.end(); i++) {
        cout << "previsit numer node " << i->first << " is : " 
        << preNum[i->first] << endl ;
        
        cout << "postvisit numer node " << i->first << " is : " 
        << postNum[i->first] << endl ;
        
        cout << "===================\n" ;
    }
}
//end
int main() {

    Graph g ;
    
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    g.addEdge(3, 1);
    g.addEdge(4, 5);
    g.addEdge(5, 6);
    g.addEdge(6, 4);

    g.DFS() ;
    g.showPrePost();
    
    
    
    return 0;
}
